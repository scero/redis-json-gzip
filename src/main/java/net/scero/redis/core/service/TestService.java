package net.scero.redis.core.service;

import lombok.extern.slf4j.Slf4j;
import net.scero.redis.core.redis.repository.FooRepository;
import net.scero.redis.entity.FooEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/*
 *
 *
 * @author joseluis.nogueira on 07/08/2019
 */
@Service
@Slf4j
public class TestService {
  @Autowired
  private FooRepository fooRepository;

  @Autowired
  private RedisTemplate redisTemplate;

  @PostConstruct
  public void setUp() {
    method1();
    method2();
    FooEntity fooEntity = recover();
    log.info(fooEntity.toString());
  }

  private void method1() {
    FooEntity fooEntity = new FooEntity();
    fooEntity.setId(1l);
    fooEntity.setName("Jose Luis");
    fooEntity.setAge(35);

    fooRepository.save(fooEntity);
  }

  private void method2() {
    FooEntity fooEntity = new FooEntity();
    fooEntity.setId(2l);
    fooEntity.setName("Jose Luis");
    fooEntity.setAge(35);

    redisTemplate.opsForSet().add("key-test", fooEntity);
  }

  private FooEntity recover(){
    return (FooEntity) redisTemplate.opsForSet().pop("key-test");
  }
}
