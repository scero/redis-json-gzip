package net.scero.redis.core.redis.repository;

import net.scero.redis.entity.FooEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * CRUD repository interface of RateEntity
 * @author dmontero
 *
 */
@Repository
public interface FooRepository extends CrudRepository<FooEntity, String> {

}
