package net.scero.redis.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

/*
 *
 *
 * @author joseluis.nogueira on 07/08/2019
 */
@Data
@RedisHash("FooTestKey")
public class FooEntity {
  @Id
  private long id;
  private String name;
  private int age;
}
