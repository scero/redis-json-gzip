package net.scero.redis.db.configuration;

import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * <p>
 * <p>
 * <p>
 * <p>
 *
 * @author cjrequena
 * @version 1.0
 * @see
 * @since JDK1.8
 */
@Data
@Log4j2
@Configuration
@EnableRedisRepositories
public class RedisConfiguration {
  @Bean
  @Primary
  RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory rcf) {
    RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(rcf);
    template.setKeySerializer(new StringRedisSerializer());
    template.setValueSerializer(new JsonGzipRedisSerializer());

    return template;
  }

  static class JsonGzipRedisSerializer implements RedisSerializer<Object> {

    private final ObjectMapper om;

    public JsonGzipRedisSerializer() {
      this.om = new ObjectMapper().enableDefaultTyping(DefaultTyping.NON_FINAL, As.PROPERTY);
    }

    @Override
    public byte[] serialize(Object t) throws SerializationException {
      ByteArrayOutputStream obj = new ByteArrayOutputStream();
      try (GZIPOutputStream out = new GZIPOutputStream(obj)) {
        out.write(om.writeValueAsBytes(t));
      } catch (IOException e) {
        log.error("Error serializing object", e);
      }

      return obj.toByteArray();
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
      if (bytes == null) {
        return null;
      }

      Object object;
      try (GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(bytes))) {
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis));
        String line;
        String outStr = "";
        while ((line = bf.readLine()) != null) {
          outStr += line;
        }
        object = om.readValue(outStr, Object.class);
      } catch (IOException e) {
        log.error("Error deserializing object", e);
        object = null;
      }

      return object;
    }
  }
}
